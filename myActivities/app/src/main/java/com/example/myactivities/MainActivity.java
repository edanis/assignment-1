package com.example.myactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button SecondPageButton;
    private TextView displayMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SecondPageButton = findViewById(R.id.btnSubmit);
        SecondPageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("MsgOne","First activity says hello");
                intent.putExtra("MsgTwo", "Hola From first act");

                startActivity(intent);
            }
        });
        Bundle myextras = getIntent().getExtras();

        displayMessage = findViewById(R.id.textViewFirstid);

        if(myextras != null) {
            String myMessage = myextras.getString("MsgThree");
            displayMessage.setText(myMessage);
        }
    }
}
