package com.example.myactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private TextView displayMessage;
    private Button FirstPageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        FirstPageButton = findViewById(R.id.btnSubmit2);
        FirstPageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, MainActivity.class);
                intent.putExtra("MsgThree","Second activity says hey");
                startActivity(intent);
            }
        });

        Bundle myextras = getIntent().getExtras();

        displayMessage = findViewById(R.id.txtViewSecondid);

        if(myextras != null) {
            String myMessage = myextras.getString("MsgOne");
            displayMessage.setText(myMessage);
        }
    }
}
