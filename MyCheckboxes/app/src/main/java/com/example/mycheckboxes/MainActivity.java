package com.example.mycheckboxes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private CheckBox BurgatoryCheckBox;
    private CheckBox SMCheckBox;
    private CheckBox BakersfieldCheckBox;

    private Button submitButton;
    private TextView showTextViewResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BurgatoryCheckBox = findViewById(R.id.chkBoxBurgatory);
        SMCheckBox = findViewById(R.id.chkBoxSM);
        BakersfieldCheckBox = findViewById(R.id.chkBoxBakersfield);

        showTextViewResults = findViewById(R.id.TextViewResults);

        submitButton = findViewById(R.id.buttonSubmit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder stringBuilder = new StringBuilder();

                if(BurgatoryCheckBox.isChecked())
                    stringBuilder.append(BurgatoryCheckBox.getText().toString() + " is: " + BurgatoryCheckBox.isChecked() + "\n");
                else if(SMCheckBox.isChecked())
                    stringBuilder.append(SMCheckBox.getText().toString() + " is: " + SMCheckBox.isChecked() + "\n");
                else if(BakersfieldCheckBox.isChecked())
                    stringBuilder.append(BakersfieldCheckBox.getText().toString() + " is: " + BakersfieldCheckBox.isChecked() + "\n");
                else
                    stringBuilder.append("Nothing was Selected \n" + "Please make a selection");

                // make text view visible
                showTextViewResults.setVisibility(View.VISIBLE);
                // Display results
                showTextViewResults.setText(stringBuilder);
            }
        });
    }
}
